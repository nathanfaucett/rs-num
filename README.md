num
=====

generic number trait

```rust

use num::Num;

fn add<T: Num>(a: T, b: T) -> T {
  a + b
}
```
