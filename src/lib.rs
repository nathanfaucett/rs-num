#![feature(alloc)]
#![no_std]


extern crate alloc;

extern crate bounded;
extern crate to_primitive;
extern crate from_primitive;
extern crate one;
extern crate zero;


mod num;


pub use num::Num;
